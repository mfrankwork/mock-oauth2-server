const express = require('express')
const router = express.Router() // Instantiate a new router
const DebugControl = require('../utilities/debug.js')

router.get('/subjects/:subjectId/attributes', (req,res) => {  // Successfully reached if can hit this :)
  DebugControl.log.variable({name: 'res.locals.oauth.token', value: res.locals.oauth.token})
  DebugControl.log.variable({name: 'req.params', value: req.params})
  res.json({
    "attributes": {
      "dn": [ req.params.subjectId ],
      "primaryorgufat": [ "Park Place/ABC/DEF/GHI/JKL/MNO/PQR/STU/VWX/YZ" ]
    }
  })
})

module.exports = router
